import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { CardForm } from '@shared/interface/card.interface';
import * as fromApp from '../app.reducer';
import * as PaymentActions from '../cards/store/payment-list.actions';
import { CardMapperService } from '../mapper/card.mapper';
import { Router } from '@angular/router';
import { ROUTES_CONFIG } from '@shared/constants/routes.const';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  private url = 'https://card-c2988.firebaseio.com/payment.json';

  constructor(
    private http: HttpClient,
    private store: Store<fromApp.AppState>,
    private cardMapperService: CardMapperService,
    private router: Router
  ) { }

  handleError() {
    this.router.navigate([`${ROUTES_CONFIG.ERROR}`]);
  }

  postPayment(payload: CardForm, date: string): Observable<any> {
    const payment: CardForm = { ...payload, date };
    return this.http.post(this.url, payment)
      .pipe(take(1),
        map((data: any) => {
          this.store.dispatch(new PaymentActions.AddPayment({ id: data.name, card: payment }));
        }),
        catchError(errorRes => {
          this.handleError();
          return of(errorRes);
        }));
  }

  getPaymentList(): Observable<any> {
    return this.http.get(this.url)
      .pipe(take(1),
        map(data => {
          const payment = this.cardMapperService.transform(data);
          this.store.dispatch(new PaymentActions.FetchPayments(payment));
          return payment;
        }),
        catchError(errorRes => {
          this.handleError();
          return of(errorRes);
        }));
  }
}
