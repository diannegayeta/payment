import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

import { Card, CardForm } from '@shared/interface/card.interface';
import * as fromApp from '../app.reducer';
import * as CardActions from '../cards/store/card-list.actions';
import { CardMapperService } from '../mapper/card.mapper';
import { Router } from '@angular/router';
import { ROUTES_CONFIG } from '@shared/constants/routes.const';



@Injectable({
  providedIn: 'root'
})
export class CardService {
  private url = 'https://card-c2988.firebaseio.com/cards';

  constructor(
    private http: HttpClient,
    private store: Store<fromApp.AppState>,
    private cardMapperService: CardMapperService,
    private router: Router
  ) { }

  handleError() {
    this.router.navigate([`${ROUTES_CONFIG.ERROR}`]);
  }

  saveCard(payload: CardForm): Observable<any> {
    return this.http.post(`${this.url}.json`, payload)
      .pipe(take(1),
        map((data: any) => {
          this.store.dispatch(new CardActions.AddCard({id: data.name, card: payload}));
        }),
        catchError(errorRes => {
          this.handleError();
          return of(errorRes);
        }));
  }

  updateCard(payload: CardForm, id: string): Observable<any> {
    return this.http.patch(`${this.url}/${id}.json`, payload)
      .pipe(take(1),
        map(() => {
          this.store.dispatch(new CardActions.UpdateCard({ id, newCard: payload }));
        }),
        catchError(errorRes => {
          this.handleError();
          return of(errorRes);
        }));
  }

  deleteCard(id: string): Observable<any> {
    return this.http.delete(`${this.url}/${id}.json`)
      .pipe(take(1),
        map(() => {
          this.store.dispatch(new CardActions.DeleteCard(id));
        }),
        catchError(errorRes => {
          this.handleError();
          return of(errorRes);
        }));
  }

  getCardList(): Observable<any> {
    return this.http.get(`${this.url}.json`)
      .pipe(take(1),
        map(data => {
          const cards = this.cardMapperService.transform(data);
          this.store.dispatch(new CardActions.FetchCards(cards));
          return cards;
        }),
        catchError(errorRes => {
          this.handleError();
          return of(errorRes);
        }));
  }
}
