import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ErrorPageComponent } from '@shared/components/error-page/error-page.component';
import { ROUTES_CONFIG } from '@shared/constants/routes.const';


const routes: Routes = [
  {path: '', redirectTo: ROUTES_CONFIG.CARDS, pathMatch: 'full'},
  {
    path: ROUTES_CONFIG.CARDS,
    loadChildren : () => import('./cards/cards.module').then(m => m.CardsModule),
  },
  {
    path: ROUTES_CONFIG.PAYMENT,
    loadChildren : () => import('./cards/cards.module').then(m => m.CardsModule),
  },
  {
    path: ROUTES_CONFIG.ERROR,
    component: ErrorPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
