import { Injectable } from '@angular/core';
import { Card } from '@shared/interface/card.interface';

@Injectable({
  providedIn: 'root'
})
export class CardMapperService {
  constructor() { }

  transform(data: any): Card[] {
    let cards: Card[] = [];
    if (data) {
      cards = Object.keys(data).map((key) => {
        const card: Card = {
          id: key,
          card: data[key]
        };
        return card;
      });
    }
    return [...cards];
  }
}
