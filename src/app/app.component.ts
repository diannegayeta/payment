import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CardService } from './services/card.service';
import { PaymentService } from './services/payment.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  private subscription = new Subject();

  constructor(
    private cardService: CardService,
    private paymentService: PaymentService,
  ) { }

  ngOnInit() {
    this.cardService.getCardList()
      .pipe(takeUntil(this.subscription))
      .subscribe((data) => {
        // console.log(data);
      });
    this.paymentService.getPaymentList()
      .pipe(takeUntil(this.subscription))
      .subscribe((data) => {
        // console.log(data);
      });
  }

  ngOnDestroy() {
    this.subscription.next();
    this.subscription.complete();
  }

}
