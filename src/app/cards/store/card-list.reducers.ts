import { Card } from '@shared/interface/card.interface';
import * as CardActions from './card-list.actions';

export interface State {
  cards: Card[];
}

const initialState: State = {
  cards: []
};

export function cardListReducer(
  state = initialState,
  action: CardActions.CardActions
) {
  switch (action.type) {
    case CardActions.FETCH_CARDS:
      return {
        ...state,
        cards: [...action.payload]
      };
    case CardActions.ADD_CARD:
      return {
        ...state,
        cards: [...state.cards, action.payload]
      };
    case CardActions.UPDATE_CARD:
      const index = state.cards.findIndex(c => c.id === action.payload.id)
      const updatedCard = {
        ...state.cards[index],
        card: action.payload.newCard
      };
      const updatedCards = [...state.cards];
      updatedCards[index] = updatedCard;
      return {
        ...state,
        cards: updatedCards
      };
    case CardActions.DELETE_CARD:
      return {
        ...state,
        cards: state.cards.filter((card) => {
          return card.id !== action.payload;
        })
      };
    default:
      return state;
  }
}
