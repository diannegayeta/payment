import { Card } from '@shared/interface/card.interface';
import * as PaymentListActions from './payment-list.actions';

export interface State {
  payments: Card[];
}

const initialState: State = {
  payments: []
};

export function paymentListReducer(
  state = initialState,
  action: PaymentListActions.PaymentListActions
) {
  switch (action.type) {
    case PaymentListActions.FETCH_PAYMENTS:
      return {
        ...state,
        payments: [...action.payload]
      };
    case PaymentListActions.ADD_PAYMENT:
      return {
        ...state,
        payments: [...state.payments, action.payload]
      };
    default:
      return state;
  }
}
