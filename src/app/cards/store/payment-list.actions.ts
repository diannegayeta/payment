import { Action } from '@ngrx/store';
import { Card } from '@shared/interface/card.interface';

export const FETCH_PAYMENTS = 'FETCH_PAYMENTS';
export const ADD_PAYMENT = 'ADD_PAYMENT';

export class FetchPayments implements Action {
  readonly type = FETCH_PAYMENTS;

  constructor(public payload: Card[]) {}
}

export class AddPayment implements Action {
  readonly type = ADD_PAYMENT;

  constructor(public payload: Card) {}
}

export type PaymentListActions =
  FetchPayments |
  AddPayment;
