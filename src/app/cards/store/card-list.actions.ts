import { Action } from '@ngrx/store';
import { CardForm, Card } from '@shared/interface/card.interface';


export const FETCH_CARDS = 'FETCH_CARDS';
export const ADD_CARD = 'ADD_CARD';
export const UPDATE_CARD = 'UPDATE_CARD';
export const DELETE_CARD = 'DELETE_CARD';

export class FetchCards implements Action {
  readonly type = FETCH_CARDS;

  constructor(public payload: Card[]) {}
}

export class AddCard implements Action {
  readonly type = ADD_CARD;

  constructor(public payload: Card) {}
}

export class UpdateCard implements Action {
  readonly type = UPDATE_CARD;

  constructor(public payload: { id: string; newCard: CardForm }) {}
}

export class DeleteCard implements Action {
  readonly type = DELETE_CARD;

  constructor(public payload: string) {}
}

export type CardActions =
  FetchCards |
  AddCard |
  UpdateCard |
  DeleteCard;
