import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ROUTES_CONFIG } from '@shared/constants/routes.const';
import { CardFormComponent } from './card-form/card-form.component';
import { CardsComponent } from './cards.component';


const routes: Routes = [
  {path: '', redirectTo: ROUTES_CONFIG.LIST, pathMatch: 'full'},
  {
    path: ROUTES_CONFIG.PAY,
    component: CardFormComponent,
  },
  {
    path: ROUTES_CONFIG.HISTORY,
    component: CardsComponent,
    data: {payment: true}
  },
  {
    path: ROUTES_CONFIG.LIST,
    component: CardsComponent
  },
  {
    path: ROUTES_CONFIG.VIEW,
    component: CardFormComponent,
    data: {view: true}
  },
  {
    path: ROUTES_CONFIG.UPDATE,
    component: CardFormComponent,
    data: {card: true, update: true}
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CardsRoutingModule { }
