import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { ROUTES_CONFIG } from '@shared/constants/routes.const';
import { Card } from '@shared/interface/card.interface';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as fromApp from '../app.reducer';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit, OnDestroy {
  cards: Card[];
  isPayment = false;
  title: string;
  private subscription = new Subject();

  constructor(
    private store: Store<fromApp.AppState>,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => this.isPayment = !!data.payment);
    this.isPayment ? this.getPaymentHistory() : this.getCardList();
  }

  getCardList() {
    this.store.select('cardList')
      .pipe(takeUntil(this.subscription))
      .subscribe(data => {
        this.cards = data && data.cards;
        this.title = 'Card List';
      });
  }

  getPaymentHistory() {
    this.store.select('paymentList')
      .pipe(takeUntil(this.subscription))
      .subscribe(data => {
        this.cards = data && data.payments;
        this.title = 'Payment History';
      });
  }

  onClick(id: string) {
    if (this.isPayment) {
      this.router.navigate([`${ROUTES_CONFIG.PAYMENT}`, `${ROUTES_CONFIG.VIEW}`], { queryParams: { id } });
    } else {
      this.router.navigate([`${ROUTES_CONFIG.CARDS}`, `${ROUTES_CONFIG.UPDATE}`], { queryParams: { id } });
    }
  }

  ngOnDestroy() {
    this.subscription.next();
    this.subscription.complete();
  }

}
