import { Component, Input, OnInit } from '@angular/core';
import { CardForm } from '@shared/interface/card.interface';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() card: CardForm;
  @Input() id: string;

  constructor() { }

  ngOnInit(): void {
  }

}
