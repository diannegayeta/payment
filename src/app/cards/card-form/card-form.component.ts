import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import * as _moment from 'moment';
import { Moment } from 'moment';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

import { CardForm } from '@shared/interface/card.interface';
import { dateValidator, getDate, getMonth, regexValidator } from '@shared/utils/custom-validators.utils';
import { CardService } from 'src/app/services/card.service';
import { PaymentService } from 'src/app/services/payment.service';
import * as fromApp from '../../app.reducer';
import { ROUTES_CONFIG } from '@shared/constants/routes.const';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '@shared/components/dialog/dialog.component';

const moment = _moment;

@Component({
  selector: 'app-card-form',
  templateUrl: './card-form.component.html',
  styleUrls: ['./card-form.component.scss']
})
export class CardFormComponent implements OnInit, OnDestroy {
  cardFormGroup: FormGroup;
  card: CardForm;
  view = false;
  update = false;
  private cardId: string;
  private subscription = new Subject();
  private horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  private verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private paymentService: PaymentService,
    private cardService: CardService,
    private snackBar: MatSnackBar,
    private store: Store<fromApp.AppState>,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.view = !!data.view;
      this.update = !!data.update;
    });
    this.activatedRoute.queryParams.subscribe((res) => {
      this.cardId = res.id;
      if (this.cardId) {
        this.view ? this.selectPayment() : this.selectCard();
      } else {
        this.initForm();
      }
    });
  }

  selectCard() {
    this.store.select('cardList')
      .pipe(takeUntil(this.subscription))
      .subscribe(data => {
        const item = data && data.cards.find(d => d.id === this.cardId);
        this.card = item && item.card;
        this.initForm();
      });
  }

  selectPayment() {
    this.store.select('paymentList')
      .pipe(takeUntil(this.subscription))
      .subscribe(data => {
        const item = data && data.payments.find(d => d.id === this.cardId);
        this.card = item && item.card;
        this.initForm();
      });
  }


  initForm(): void {
    const cardNumber = this.card && this.card.cardNumber;
    const cardHolderName = this.card && this.card.cardHolderName;
    const cardExpiryDate = this.card && this.card.cardExpiryDate || getMonth(1);
    const cardCCV = this.card && this.card.cardCCV;
    const cardPayAmount = this.card && this.card.cardPayAmount;

    this.cardFormGroup = new FormGroup({
      cardNumber: new FormControl(
        {value: cardNumber, disabled: this.view }, Validators.required),
      cardHolderName: new FormControl(
        {value: cardHolderName, disabled: this.view}, Validators.required),
      cardExpiryDate: new FormControl(
        {value: moment(cardExpiryDate), disabled: this.view}, [Validators.required, dateValidator()]),
      cardCCV: new FormControl(
        {value: cardCCV, disabled: this.view}, regexValidator(new RegExp('^[0-9]{3}$'), { INVALID_CCV: true })),
      cardPayAmount: new FormControl(
        {value: cardPayAmount, disabled: this.view},
        [Validators.required, regexValidator(new RegExp('^[1-9]\\d*$'), { INVALID_AMOUNT: true })]),
    });
  }

  submit(type: string) {
    const payload: CardForm = {
      ...this.cardFormGroup.value,
      cardExpiryDate: new Date(this.cardFormGroup.value.cardExpiryDate).toISOString()
    };
    switch (type) {
      case 'pay': this.postPayment(payload, type);
                  break;
      case 'save': this.saveCard(payload, type);
                   break;
      case 'update': this.updateCard(payload, type);
                     break;
      case 'delete': this.deleteCard(type);
                     break;
    }
  }

  postPayment(payload: CardForm, type: string) {
    this.paymentService.postPayment(payload, getDate())
      .pipe(takeUntil(this.subscription))
      .subscribe(data => {
        this.openSnackBar(type, 'Payment Successful');
      });
  }

  saveCard(payload: CardForm, type: string) {
    const savePayload: CardForm = {
      ...payload,
      cardPayAmount: null
    };
    this.cardService.saveCard(savePayload)
      .pipe(takeUntil(this.subscription))
      .subscribe(data => {
        this.openSnackBar(type, 'Saved Successfully');
      });
  }

  updateCard(payload: CardForm, type: string) {
    this.cardService.updateCard(payload, this.cardId)
      .pipe(takeUntil(this.subscription))
      .subscribe(data => {
        this.openSnackBar(type, 'Updated Successfully');
      });
  }

  deleteCard(type: string) {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'Warning!',
        content: 'Are you sure you want to delete this card?',
        confirmBtnText: 'OK',
        cancelBtnText: 'Cancel',
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.cardService.deleteCard(this.cardId)
          .pipe(takeUntil(this.subscription))
          .subscribe(data => {
            this.openSnackBar(type, 'Card Deleted');
          });
      }
    });
  }

  getError(field: string) {
    return this.cardFormGroup.controls[field].dirty &&
      this.cardFormGroup.controls[field].errors;
  }

  getControl(control: string) {
    return this.cardFormGroup.controls[control];
  }

  chosenYearHandler(normalizedYear: Moment) {
    const cardExpiryDate = this.getControl('cardExpiryDate');
    cardExpiryDate.markAsDirty();
    const ctrlValue = cardExpiryDate.value;
    ctrlValue.year(normalizedYear.year());
    cardExpiryDate.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const cardExpiryDate = this.getControl('cardExpiryDate');
    const ctrlValue = cardExpiryDate.value;
    ctrlValue.month(normalizedMonth.month());
    cardExpiryDate.setValue(ctrlValue);
    datepicker.close();
  }

  openSnackBar(type: string, msg: string) {
    const snackBarRef = this.snackBar.open(msg, 'OK', {
      duration: 1000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });

    snackBarRef.afterDismissed().subscribe(() => {
      if (type === 'save' ) {
        return;
      }
      if (type === 'pay') {
        this.router.navigate([`${ROUTES_CONFIG.PAYMENT}`, `${ROUTES_CONFIG.HISTORY}`]);
      } else {
        this.router.navigate([`${ROUTES_CONFIG.CARDS}`, `${ROUTES_CONFIG.LIST}`]);
      }
    });
  }

  ngOnDestroy() {
    this.subscription.next();
    this.subscription.complete();
  }
}
