import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardsRoutingModule } from './cards-routing.module';
import { CardsComponent } from './cards.component';
import { CardComponent } from './card/card.component';
import { CardFormComponent } from './card-form/card-form.component';
import { AppMaterialModule } from '../app-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';
import { DialogComponent } from '@shared/components/dialog/dialog.component';



@NgModule({
  declarations: [CardsComponent, CardComponent, CardFormComponent],
  imports: [
    CommonModule,
    CardsRoutingModule,
    SharedModule.forRoot(),
    AppMaterialModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    DialogComponent
  ]
})
export class CardsModule { }
