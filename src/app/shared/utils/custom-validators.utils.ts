import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import * as _moment from 'moment';

const moment = _moment;

export function regexValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    if (!control.value) {
      return null;
    }
    const valid = regex.test(control.value);
    return valid ? null : error;
  };
}

export function dateValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    if (!control.value) {
      return null;
    }
    const valid = moment(control.value).format('YYYY/MM') > getMonth();
    return valid ? null : {INVALID_DATE: true};
  };
}

export function getMonth(month = 0) {
  const date = new Date();
  let currentMonth = date.getMonth() + 1 + month;
  let currentYear = date.getFullYear();
  if (currentMonth > 12) {
    currentMonth = currentMonth - 12;
    currentYear = ++currentYear;
  }
  return  `${currentYear}/${currentMonth}`;
}

export function getDate() {
  return new Date().toISOString();
}

