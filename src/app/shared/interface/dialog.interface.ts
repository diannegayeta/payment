export interface DialogData  {
  title: string;
  content: string;
  confirmBtnText: string;
  cancelBtnText: string;
}
