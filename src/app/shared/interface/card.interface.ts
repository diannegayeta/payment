export interface CardForm {
  cardNumber: string;
  cardHolderName: string;
  cardExpiryDate: Date;
  cardCCV: string;
  cardPayAmount: number;
  date?: string;
}

export interface Card {
  id: string;
  card: CardForm;
}

