import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorComponent } from './components/error/error.component';
import { HeaderComponent } from './components/header/header.component';
import { AppMaterialModule } from '../app-material.module';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import { DATE_FORMATS } from './constants/date.const';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { DialogComponent } from './components/dialog/dialog.component';


@NgModule({
  declarations: [ErrorComponent, HeaderComponent, SpinnerComponent, ErrorPageComponent, DialogComponent],
  imports: [
    CommonModule,
    AppMaterialModule
  ],
  exports: [
    ErrorComponent,
    HeaderComponent,
    ErrorPageComponent,
    DialogComponent
  ],
  entryComponents: [
    DialogComponent,
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        {
          provide: DateAdapter,
          useClass: MomentDateAdapter,
          deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
        },
        {provide: MAT_DATE_FORMATS, useValue: DATE_FORMATS},
      ],
    };
  }
}
