
export const ROUTES_CONFIG = {
  CARDS: 'cards',
  LIST: 'list',
  VIEW: 'view',
  UPDATE: 'update',
  PAYMENT: 'payment',
  HISTORY: 'history',
  PAY: 'pay',
  ERROR: 'error'
};
