
export const FIELD_ERRORS = {
  INVALID_AMOUNT: 'Entered amount is invalid',
  INVALID_DATE: 'Invalid date',
  required: 'This field is required',
  INVALID_CCV: 'Invalid CCV'
};
