import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { FIELD_ERRORS } from '@shared/constants/errors.const';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ErrorComponent implements OnInit, OnChanges {
  @Input() errors: ValidationErrors;
  error: string;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    this.error = '';
    if (this.errors) {
      const key  = Object.keys(this.errors);
      this.error = FIELD_ERRORS[key[0]];
    }
  }

}
