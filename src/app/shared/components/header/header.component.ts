import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { ROUTES_CONFIG } from '@shared/constants/routes.const';
import { Location } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  paymentList = false;
  private url: string;

  constructor(
    private router: Router,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        this.url = e.url;
        this.paymentList = false;
        if (this.url.includes('payment')) {
          this.paymentList = true;
        }
      }
    });
  }

  switchView() {
    // this.paymentList = !this.paymentList;
    if (this.paymentList) {
      this.router.navigate([ROUTES_CONFIG.CARDS, ROUTES_CONFIG.LIST]);
    } else {
      this.router.navigate([ROUTES_CONFIG.PAYMENT, ROUTES_CONFIG.HISTORY]);
    }
  }

  onPay() {
    const id = this.url.split('?id=')[1];
    if (id && this.showMakePayment()) {
      this.router.navigate([ROUTES_CONFIG.CARDS, ROUTES_CONFIG.PAY], { queryParams: { id } });
    } else {
      this.router.navigate([ROUTES_CONFIG.PAYMENT, ROUTES_CONFIG.PAY], { queryParams: { id } });
    }
  }

  showBack() {
    return this.url && this.url !== '/' && !this.url.includes('list') && !this.url.includes('history') ? true : false;
  }

  onBack() {
    this.location.back();
  }

  showMakePayment() {
    return this.url && this.url.includes('payment/view') ? false : true;
  }

}
