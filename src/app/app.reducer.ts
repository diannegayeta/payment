import { ActionReducerMap } from '@ngrx/store';

import * as fromCardList from './cards/store/card-list.reducers';
import * as fromPaymentList from './cards/store/payment-list.reducers';

export interface AppState {
  cardList: fromCardList.State;
  paymentList: fromPaymentList.State;
}

export const appReducer: ActionReducerMap<AppState> = {
  cardList: fromCardList.cardListReducer,
  paymentList: fromPaymentList.paymentListReducer,
};
